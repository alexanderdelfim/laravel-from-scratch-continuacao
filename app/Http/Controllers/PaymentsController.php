<?php


namespace App\Http\Controllers;


use App\Events\ProductPurchased;
use App\Notifications\PaymentReceived;
use Illuminate\Support\Facades\Notification;

class PaymentsController
{
    public function create()
    {
        return view('payments.create');
    }

    public function store()
    {
        ProductPurchased::dispatch('toy');
        Notification::send(request()->user(), new PaymentReceived(2000));

    }
}
