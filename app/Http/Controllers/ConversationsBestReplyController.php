<?php

namespace App\Http\Controllers;

use App\Reply;
use Illuminate\Http\Request;

class ConversationsBestReplyController extends Controller
{
    public function store(Reply $reply)
    {
        $conversation = $reply->conversation;
        $this->authorize('update', $conversation);

        $this->authorize('update', $reply->conversation);

        $reply->conversation->setBestReply($reply);

        return back();
    }
}
