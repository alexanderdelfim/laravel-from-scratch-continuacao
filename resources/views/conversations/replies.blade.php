@foreach($conversation->replies as $reply)

    <div class="container">

        <header style="display: flex; justify-content: space-between;">
            <p class="m-0"><strong>{{ $reply->user->name }} said...</strong></p>
            @if($reply->isBest())
                <span style="color: green">Best Reply!!</span>
            @endif
        </header>

        {{ $reply->body }}

        @can('update', $conversation)
            <form method="post" action="/best-replies/{{ $reply->id }}">
                @csrf
                <button
                    type="submit"
                    class="btn p-0 text-muted"
                >Best reply?
                </button>
            </form>
        @endcan

        @continue($loop->last)

        <hr>
    </div>
@endforeach
