@extends('layout')
@section('content')
    <div class="flex-center position-ref full-height">
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ route('login') }}">Login</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                    @endif
                @endauth
            </div>
        @endif
        <div>
            <h1>My site</h1>
            @can('edit_forum')
                <li>
                    <a href="#">Edit Forum</a>
                </li>
            @endcan

            @can('view_reports')
                <li>
                    <a href="/reports">View Reports</a>
                </li>
            @endcan
        </div>

    </div>
@endsection
