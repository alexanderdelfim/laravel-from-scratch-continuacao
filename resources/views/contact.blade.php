@extends('layout')
@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
@endsection
@section('content')
<body class="container">
    <div class="column">
        <div class="column"></div>
        <div class="has-background-grey-lighter column">
            <form action="/contact" method="post">
                @csrf
                <div class="field">
                    <div class="control">
                        <label for="email" class="label">Email address</label>
                        <input type="text" id="email" name="email" class="input is-info">
                        @error('email')
                        <div class="has-text-danger is-size-7 mt-2">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-info">Email Me</button>
                    </div>
                </div>
                @if(session('message'))
                    <p class="has-text-success-dark mt-2">
                        {{ session('message') }}
                    </p>
                @endif
            </form>
        </div>
        <div class="column"></div>
    </div>
</body>
@endsection
