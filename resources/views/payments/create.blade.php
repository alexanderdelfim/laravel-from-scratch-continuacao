@extends('layout')
@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.0/css/bulma.min.css">
@endsection
@section('content')
    <body class="container">
    <div class="column">
        <div class="column"></div>
        <div class="has-background-grey-lighter column">
            <form action="/payments" method="post">
                @csrf
                <button class="button is-link" type="submit">Pay now</button>
            </form>
        </div>
        <div class="column"></div>
    </div>
    </body>
@endsection
